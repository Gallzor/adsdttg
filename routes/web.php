<?php

use App\Http\Controllers\GameController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\ScoreController;
use App\Models\Game;
use App\Models\Player;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Login route
Route::get('/', function () {
    return view('welcome');
});
//Authentication for routes
Auth::routes();

//Home Route
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Game route
Route::resource('game', GameController::class);

//Scoreboard route
Route::resource('scoreboard', ScoreController::class);

//Players route
Route::resource('players', PlayerController::class);

////Users route
//Route::resource('game', GameController::class);
