<!DOCTYPE html>
<html>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>



<div class="w3-container w3-green">
    @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
                <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                @endif
            @endauth
                <a href="{{ url('/') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Back to home</a>
        </div>
    @endif
    <h1>Project's Player Information</h1>
    <p>sup</p>
</div>

<div class="w3-row-padding">
    <div class="w3-third">
        <h2>Placeholder 1</h2>
        <p>(This needs to be filled in someday)</p>
        <p>A slice of heaven. O for awesome, this chocka full cuzzie is as rip-off as a cracker.
            Meanwhile, in behind the bicycle shed, Hercules Morse, as big as a horse
            and Mrs Falani were up to no good with a bunch of crook pikelets.
            Meanwhile, at the black singlet woolshed party, not even au, sort your drinking out.</p>

    </div>

    <div class="w3-third">
        <h2>Placeholder 2</h2>
        <p>(This needs to be filled in someday)</p>
        <p>A slice of heaven. O for awesome, this chocka full cuzzie is as rip-off as a cracker.
            Meanwhile, in behind the bicycle shed, Hercules Morse, as big as a horse
            and Mrs Falani were up to no good with a bunch of crook pikelets.
            Meanwhile, at the black singlet woolshed party, not even au, sort your drinking out.</p>
    </div>

    <div class="w3-third">
        <h2>Placeholder 3</h2>
        <p>(This needs to be filled in someday)</p>
        <p>A slice of heaven. O for awesome, this chocka full cuzzie is as rip-off as a cracker.
            Meanwhile, in behind the bicycle shed, Hercules Morse, as big as a horse
            and Mrs Falani were up to no good with a bunch of crook pikelets.
            Meanwhile, at the black singlet woolshed party, not even au, sort your drinking out.</p>
    </div>
</div>

</body>
</html>
