<!DOCTYPE html>
<html>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>



<div class="w3-container w3-green">
    @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
                <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                @endif
            @endauth
                <a href="{{ url('/') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Back to home</a>
        </div>
    @endif
    <h1>Project's Scoreboard</h1>
    <p>sup</p>
</div>

<div class="w3-row-padding">
    <div class="w3-third">
        <h2>Scoreboard 1</h2>
        <p>Highest score of all players</p>
        <table>
           <tr>
               <th>Rank</th>
               <th>Avatar</th>
               <th>Player</th>
               <th>Highscore</th>
{{--               <th>Total games played</th>--}}
           </tr>
            @foreach($scores->sortByDesc('highscore') as $score)
            <tr>
                <td>{{$rank++}}</td>
                <td> <img class="bd-placeholder-img" alt= "idk" src="{{ asset("storage/{$score->player->avatar}") }}"></td>
                <td>{{$score->player->username}}</td>
                <td>{{$score->highscore}}</td>
{{--                <td>{{$score->}}</td>--}}
            </tr>
            @endforeach
        </table>

    </div>

    <div class="w3-third">
        <h2>Scoreboard 2</h2>
        <p>Highest score in Country/Province (Norway)</p>
        <table>
            <tr>
                <th>Country</th>
                <th>Rank</th>
                <th>Avatar</th>
                <th>Player</th>
                <th>Highscore</th>
            </tr>
            @foreach($scores->where('player.province', 'Norway')->sortByDesc('highscore') as $score)
                <tr>
                    <th>{{$score->player->province}}</th>
                    <td>{{$rankprov++}}</td>
                    <td> <img class="bd-placeholder-img" alt= "idk" src="{{ asset("storage/{$score->player->avatar}") }}"></td>
                    <td>{{$score->player->username}}</td>
                    <td>{{$score->highscore}}</td>
                </tr>
        @endforeach
        </table>
    </div>

    <div class="w3-third">
        <h2>Scoreboard 3</h2>
        <p>Players with the most played games</p>
        <table>
            <tr>
                <th>Rank</th>
                <th>Games Played</th>
                <th>Avatar</th>
                <th>Player</th>
                <th>Country</th>

            </tr>
            @foreach($scores->sortByDesc('player.games_played') as $score)
                <tr>
                    <td>{{$rankgames++}}</td>
                    <td>{{$score->player->games_played}}</td>
                    <td> <img class="bd-placeholder-img" alt= "idk" src="{{ asset("storage/{$score->player->avatar}") }}"></td>
                    <td>{{$score->player->username}}</td>
                    <th>{{$score->player->province}}</th>
                </tr>
            @endforeach
        </table>
    </div>
</div>

</body>
</html>
