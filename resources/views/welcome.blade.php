<!DOCTYPE html>
<html>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>



<div class="w3-container w3-green">
    @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
                <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                @endif
            @endauth
        </div>
    @endif
    <h1>Project's homepage</h1>
    <p>sup</p>
</div>

<div class="w3-row-padding">
    <div class="w3-third">
        <h2>Gamepage</h2>
        <p>This will be the page for official project</p>
        <p>(someday this will be finished)</p>
        <p>(I hope..)</p>
        <p>(..help)</p>
    </div>

    <div class="w3-third">
        <h2>About</h2>
        <p>Information page</p>
        <p>Will be filled in when project is in development</p>
    </div>

    <div class="w3-third">
        <h2><a href="{{ route('game.index') }}" class="text-md text-gray-700 dark:text-gray-500 underline">Dummy page</a></h2>
        <p>On this page I will test codes</p>
        <p>Development page lets goooooo</p>
    </div>
</div>

</body>
</html>
