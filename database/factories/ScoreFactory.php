<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Score>
 */
class ScoreFactory extends Factory
{
    private static $userID = 1;
    private static $playerID = 1;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => self::$userID++,
            'player_id' => self::$playerID++,
            'highscore' => fake()->numberBetween(30, 300),
            'updated_at' => now(),
            'created_at' => now(),
        ];
    }
}
