<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Player>
 */
class PlayerFactory extends Factory
{

    private static $scoreID = 1;
    private static $userID = 1;
    private static $countryID = 'Norway';
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'score_id' =>  self::$scoreID++,
            'user_id' =>  self::$userID++,
            'username' => fake()->userName(),
            'games_played' => fake()->numberBetween(4,99),
            'province' => self::$countryID,
            'avatar' => fake()->imageUrl(),
            'updated_at' => now(),
            'created_at' => now(),
        ];
    }
}
